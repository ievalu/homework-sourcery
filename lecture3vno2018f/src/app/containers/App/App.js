import React from 'react';
import TextField from '@material-ui/core/TextField';

import Header from 'components/Header/Header';
import Button from 'components/Button/Button';
import QuizForm from 'components/QuizForm/QuizForm';

import './app.scss';

import quiz from 'data/quiz';

const postFeedback = (...args) => {
    console.log('postFeedback args: ', args);
};

const error = '';

export default () => (
    <div className="app">
        <Header text="Lecture 3 - React fundamentals" />
        <div className="home">
            <div className="quiz-code">
                <TextField
                    id="quizCode"
                    label="Quiz Code"
                    margin="normal"
                />
            </div>
            <Button text="Start" />
        </div>
        <QuizForm
            isLoading={false}
            error={error}
            quiz={quiz}
            submitForm={postFeedback}
        />
    </div>
);
