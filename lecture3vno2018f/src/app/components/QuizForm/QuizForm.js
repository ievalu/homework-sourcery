import React from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button/Button';

import Header from 'components/Header/Header';
import Question from 'components/Question/Question';

import './quizForm.scss';
import Typography from '@material-ui/core/Typography/Typography';
import quizList from 'data/quiz';

import Results from 'components/Results/Results';
import FormValidator from '../FormValidator/FormValidator';

class QuizForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            formValid: true,
            answersCorrect: 0,
            validCheckboxes: true,
        };
    }

    /* eslint-disable react/sort-comp */
    feedback;
    /* eslint-enable */

    componentDidMount = () => {
        this.feedback = {
            quizId: this.props.quiz.id,
            answers: {},
        };
    };

    handleChange = (id, value) => {
        if (value) { this.feedback.answers[`question-${id}`] = value; } else { delete this.feedback.answers[`question-${id}`]; }
    };

    arraysEqual(a, b) {
        if (a === b) return true;
        if (a == null || b == null) return false;
        if (a.length !== b.length) return false;
        a.sort();
        b.sort();
        for (let i = 0; i < a.length; i += 1) {
            if (a[i] !== b[i]) return false;
        }
        return true;
    }

    isAnswerCorrect = (answers, questionId) => {
        let isCorrect = false;
        if (quizList.questions.find(x => x.id === questionId).type === 'CHECK') {
            if (this.state.validCheckboxes) {
                this.setState({ validCheckboxes: answers[`question-${questionId}`].length === quizList.answers.find(x => x.id === questionId).answer.length });
            }
            if (this.arraysEqual(answers[`question-${questionId}`], quizList.answers.find(x => x.id === questionId).answer)) {
                isCorrect = true;
            }
        } else if (answers[`question-${questionId}`] === quizList.answers.find(x => x.id === questionId).answer) {
            isCorrect = true;
        }
        return isCorrect;
    }

    isFormValid = () => {
        const { quiz } = this.props;
        const { answers } = this.feedback;
        this.setState({ validCheckboxes: true, answersCorrect: 0 });
        for (let i = 1; i < Object.keys(answers).length + 1; i += 1) {
            this.setState(prevState => ({
                answersCorrect: prevState.answersCorrect
                + (this.isAnswerCorrect(answers, i) ? 1 : 0),
            }));
        }
        return quiz.questions.length === Object.keys(answers).length;
    }

    handleSubmit = (e) => {
        e.preventDefault();
        const isValid = this.isFormValid();
        if (isValid) {
            this.props.submitForm(this.feedback);
        }
        this.setState({ formValid: isValid });
    };

    render() {
        const { isLoading, error, quiz } = this.props;

        return (
            <form className="quiz-form" noValidate onSubmit={this.handleSubmit}>
                <Header text={quiz.title} />
                {error && (
                    <Typography className="description" variant="h5" color="error">
                        {error}
                    </Typography>
                )}
                <FormValidator
                    formValid={this.state.formValid}
                    validCheckboxes={this.state.validCheckboxes}
                />
                {this.state.answersCorrect > 0 && (
                    <Results
                        answersCorrect={this.state.answersCorrect}
                        answersTotal={quizList.questions.length}
                    />
                )}
                <Typography className="description" variant="h5" color="inherit">
                    {quiz.description}
                </Typography>
                {quiz.questions && quiz.questions.map(q => (
                    <Question
                        key={q.id}
                        data={q}
                        onChange={value => this.handleChange(q.id, value)}
                    />
                ))}
                <div className="control-section">
                    <Button
                        disabled={isLoading || !(quiz.id)}
                        variant="contained"
                        color="primary"
                        onClick={this.handleSubmit}
                        type="submit"
                    >
                        {isLoading ? 'Saving...' : 'Submit'}
                    </Button>
                    <Button variant="contained" type="button">Cancel</Button>
                </div>
            </form>
        );
    }
}

QuizForm.propTypes = {
    isLoading: PropTypes.bool.isRequired,
    error: PropTypes.string.isRequired,
    quiz: PropTypes.shape({
        id: PropTypes.string,
        code: PropTypes.string,
        title: PropTypes.string,
        description: PropTypes.string,
        active: PropTypes.bool,
        questions: PropTypes.arrayOf(PropTypes.shape({
            id: PropTypes.number,
            text: PropTypes.string,
            type: PropTypes.string,
            answers: PropTypes.arrayOf(PropTypes.shape({
                id: PropTypes.number,
                text: PropTypes.string,
            })),
        })),
    }).isRequired,
    submitForm: PropTypes.func.isRequired,
};

export default QuizForm;
