import React from 'react';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography/Typography';

class Results extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            answersCorrect: props.answersCorrect,
            answersTotal: props.answersTotal,
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            answersCorrect: nextProps.answersCorrect,
            answersTotal: nextProps.answersTotal,
        });
    }

    render() {
        return (
            <Typography className="description" variant="h5" color="error">
                { `${this.state.answersCorrect} / ${this.state.answersTotal}`}
            </Typography>
        );
    }
}

Results.propTypes = {
    answersCorrect: PropTypes.number.isRequired,
    answersTotal: PropTypes.number.isRequired,
};

export default Results;
