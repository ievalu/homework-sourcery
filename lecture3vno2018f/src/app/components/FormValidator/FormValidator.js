import React from 'react';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography/Typography';

class FormValidator extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            formValid: props.formValid,
            validCheckboxes: props.validCheckboxes,
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            formValid: nextProps.formValid,
            validCheckboxes: nextProps.validCheckboxes,
        });
    }

    render() {
        if (!this.state.formValid) {
            return (
                <Typography className="description" variant="h5" color="error">You have not answered all of the questions</Typography>
            );
        }
        if (!this.state.validCheckboxes) {
            return (
                <Typography className="description" variant="h5" color="error">You have not checked the proper amount of checkboxes</Typography>
            );
        }
        return (
            <Typography className="description" variant="h5" color="error" />
        );
    }
}

FormValidator.propTypes = {
    formValid: PropTypes.bool.isRequired,
    validCheckboxes: PropTypes.bool.isRequired,
};

export default FormValidator;
