import React from 'react';
import PropTypes from 'prop-types';
import Radio from '@material-ui/core/Radio/Radio';
import RadioGroup from '@material-ui/core/RadioGroup/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel/FormControlLabel';

import './radioAnswer.scss';

class RadioAnswer extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            value: null,
        };
    }

    handleChange = (e) => {
        const { value } = e.target;

        this.setState({
            value,
        });

        this.props.onChange(value);
    };

    render() {
        const { answers } = this.props;

        return (
            <div className="radio-answer">
                <RadioGroup
                    aria-label="Answers"
                    className="radio-group"
                    value={this.state.value}
                    onChange={this.handleChange}
                >
                    {answers && answers.map(a => (
                        <FormControlLabel
                            key={a.id}
                            value={a.text}
                            control={<Radio />}
                            label={a.text}
                        />
                    ))}
                </RadioGroup>
            </div>
        );
    }
}

RadioAnswer.propTypes = {
    answers: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number,
        text: PropTypes.string,
    })).isRequired,
    onChange: PropTypes.func.isRequired,
};

export default RadioAnswer;
