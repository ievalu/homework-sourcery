import React from 'react';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography/Typography';

import CheckAnswer from 'components/CheckAnswer/CheckAnswer';
import RadioAnswer from 'components/RadioAnswer/RadioAnswer';
import TextAnswer from 'components/TextAnswer/TextAnswer';

import './question.scss';

const questionType = {
    SHORT: 'SHORT',
    LONG: 'LONG',
    RADIO: 'RADIO',
    CHECK: 'CHECK',
};

class Question extends React.Component {
    renderAnswers = () => {
        const { type, answers } = this.props.data;

        switch (type) {
        case questionType.SHORT: return (
            <TextAnswer
                onChange={this.props.onChange}
            />
        );
        case questionType.LONG: return (
            <TextAnswer
                multiLine
                onChange={this.props.onChange}
            />
        );
        case questionType.RADIO: return (
            <RadioAnswer
                answers={answers}
                onChange={this.props.onChange}
            />
        );
        case questionType.CHECK: return (
            <CheckAnswer
                answers={this.props.data.answers}
                onChange={this.props.onChange}
            />
        );
        default: return null;
        }
    };

    render() {
        const { text, answers } = this.props.data;

        return (
            <div className="question">
                <Typography variant="subtitle1" color="inherit">
                    {text}
                </Typography>
                {this.renderAnswers(answers)}
            </div>
        );
    }
}

Question.propTypes = {
    data: PropTypes.shape({
        text: PropTypes.string,
        type: PropTypes.string,
        answers: PropTypes.arrayOf(PropTypes.shape({
            id: PropTypes.number,
            text: PropTypes.string,
        })),
    }).isRequired,
    onChange: PropTypes.func.isRequired,
};

export default Question;
