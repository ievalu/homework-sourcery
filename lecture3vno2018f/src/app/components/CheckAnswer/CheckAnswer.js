import React from 'react';
import PropTypes from 'prop-types';
import Checkbox from '@material-ui/core/Checkbox/Checkbox';
import FormGroup from '@material-ui/core/FormGroup/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel/FormControlLabel';

class CheckAnswer extends React.Component {
    /* eslint-disable react/sort-comp */
    answers;
    /* eslint-enable */

    componentDidMount = () => {
        this.answers = {};
    };

    handleChange = (e, isChecked) => {
        const { value } = e.target;

        if (isChecked) {
            this.answers[value] = true;
        } else {
            delete this.answers[value];
        }

        this.props.onChange(Object.keys(this.answers));
    };

    render() {
        return (
            <FormGroup>
                {this.props.answers.map(a => (
                    <FormControlLabel
                        key={a.id}
                        control={(
                            <Checkbox
                                onChange={this.handleChange}
                                value={a.text}
                            />
                        )}
                        label={a.text}
                    />
                ))}
            </FormGroup>
        );
    }
}

CheckAnswer.propTypes = {
    answers: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number,
        text: PropTypes.string,
    })).isRequired,
    onChange: PropTypes.func.isRequired,
};

export default CheckAnswer;
