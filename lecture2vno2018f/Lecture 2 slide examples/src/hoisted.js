
// var and function declarations are hoisted
var a; 

function b() {
    var a = function() {} // Creates local scope
    a = 10;
    return;
}

a = 1; 
b();
console.log(a); // returns 1


