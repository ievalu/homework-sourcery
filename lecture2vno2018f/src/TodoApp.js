import tasks from '../data/tasks'

class TodoApp {
    constructor() {

        let form = document.querySelector('.js-add-task-form');
        let deleteManyButton = document.querySelector('.delete-tasks')

        form.addEventListener('submit', (e) => {
            e.preventDefault();
            let taskInput = document.querySelector('.js-add-task-input');

            this.addTask(taskInput)
        });

        deleteManyButton.addEventListener('click', () => {
            for(let i=tasks.length-1;i>=0;i--){
                if(tasks[i].checked){
                    tasks.splice(i, 1);
                };
            };
            this.renderTasks();
        });

        this.renderTasks();
    }

    addTask(taskInput) {
        tasks.push({
            title: taskInput.value,
            completed: false
        });

        this.renderTasks();
    }

    deleteTask(index) {
        tasks.splice(index, 1);
        this.renderTasks();
    }

    checkBox(index){
        tasks[index].checked = !tasks[index].checked;
    }

    renderTasks() {
        let list = document.querySelector('.js-add-task-list');

        list.innerHTML = '';

        tasks.forEach((task, index) => {
            let deleteBox = document.createElement('input');
            deleteBox.type = 'checkbox';
            deleteBox.id = index;
            deleteBox.value = task;

            let newTask = document.createElement('div');
            newTask.innerText = task.title;

            let deleteButton = document.createElement('span');
            deleteButton.className = 'glyphicon glyphicon-remove';

            deleteBox.addEventListener('change', () => {
                this.checkBox(index);
            });

            deleteButton.addEventListener('click', () => {
                this.deleteTask(index);
            });

            newTask.appendChild(deleteBox);
            newTask.appendChild(deleteButton);

            list.appendChild(newTask);
        })
    }
}

export default TodoApp;