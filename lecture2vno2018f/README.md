# README #

# Practise 1 html css #

* Lecture 2 slide examples/html-practise/

### Tasks: ###
* change color
* add more html elements
* add link to find cat pictures
* add cat pictures

# Practise 2 javascript#

* Lecture 2 slide examples/src/
* Lecture 2 slide examples/index.html

### Tasks: ###
* click things
* break things

# Practise 3 javascript project #

### download npm ###

* https://www.npmjs.com/get-npm

### install node modules ###

* in root dir run:
* npm install

### running app ##

* in root dir run:
* npm start

### open web app locally ###

* if everything went well, web app should be accesable at:
* http://localhost:8080/webpack-dev-server/

### Tasks: ###
* setup project
* add simple click action, e.g. button which is changing some element
* make simple todo list

# Homework: #
* add multiple delete option (e.g. checkboxes for each list item, and button to remove selected list items)
* send changes to lector, via email or sourcery slack
* deadline 2018-10-07 14:00